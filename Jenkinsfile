pipeline {
    agent any
    tools {
        gradle 'Gradle-6.3'
        jdk "JDK-14"
    }
    environment {
        JAVA_HOME = "${jdk}"
    }
    stages {
        stage('Initialize') {
            steps {
                script{
                    GRADLE_BUILD_DEFAULT='gradle --configure-on-demand -PBRANCH_NAME=${BRANCH_NAME} -PENV_VENDOR=${ENV_VENDOR} -PENV_BUILD_NUMBER=${BUILD_NUMBER}'
                }
                sh 'echo "JDK path = $JAVA_HOME"'
                sh 'gradle --configure-on-demand clean'
            }
        }
        stage('Code Analysis') {
            steps {
                sh "${GRADLE_BUILD_DEFAULT} -x check fullAnalysisAndCheck"
            }
        }
        stage('Tests') {
            steps {
                sh "${GRADLE_BUILD_DEFAULT} -x check test"
            }
        }
        stage('Build Module') {
            steps {
                sh "${GRADLE_BUILD_DEFAULT} -x check build"
                archiveArtifacts artifacts: 'build/libs/**/*.jar', fingerprint: true, onlyIfSuccessful: true
            }
        }
        stage('Nexus publish'){
            steps {
                withCredentials([usernamePassword(credentialsId: 'nexus_publish', passwordVariable: 'archpass', usernameVariable: 'archuser')]) {
                    sh "${GRADLE_BUILD_DEFAULT} -Parchuser=$archuser -Parchpass=$archpass -x check uploadArchives"
                }
            }
        }
        stage('Publish module') {
            when { 
                anyOf { 
                    branch 'release'; branch 'development' 
                } 
            }
            steps {
                withCredentials([usernamePassword(credentialsId: 'bitbucket-cloud', passwordVariable: 'scmpass', usernameVariable: 'scmuser')]) {
                    script {
                        MODULE_BUILD_VERSION = sh (
                            script: 'gradle --configure-on-demand -q properties | grep "version:" | awk \'{print $2}\'',
                            returnStdout: true
                        ).trim()
                    }
                    sh "echo Publishing version: ${MODULE_BUILD_VERSION}"
                    sh "curl --fail-early -u ${scmuser}:${scmpass} -X POST https://api.bitbucket.org/2.0/repositories/pidome/pidome-module-buienradar/downloads -F files=@build/libs/pidome-module-buienradar-${MODULE_BUILD_VERSION}.jar"
                }
            }
        }
    }
    post { 
        always {
            jacoco(execPattern: '**/*.exec')
            //junit '**/test-results/test/*.xml'
            recordIssues enabledForFailure: true, tools: [java(), javaDoc()]
            recordIssues enabledForFailure: true, tool: cpd(pattern: 'build/analysis/**/cpd/main.xml')
            recordIssues enabledForFailure: true, tool: checkStyle(pattern: 'build/analysis/**/checkstyle/main.xml')
            recordIssues enabledForFailure: true, tool: spotBugs(pattern: 'build/analysis/**/spotbugs/main.xml'), qualityGates: [[threshold: 1, type: 'TOTAL', unstable: true]]
            recordIssues enabledForFailure: true, tool: pmdParser(pattern: 'build/analysis/**/pmd/main.xml'), qualityGates: [[threshold: 1, type: 'TOTAL', unstable: true]]
            recordIssues enabledForFailure: true, tool: taskScanner(includePattern:'**/*.java', excludePattern:'target/**/*', highTags:'FIXME', normalTags:'TODO'), qualityGates: [[threshold: 1, type: 'TOTAL', unstable: true]]
            jiraSendBuildInfo site: 'pidome.atlassian.net'
        }
        cleanup { 
            cleanWs() 
        }
    }
}