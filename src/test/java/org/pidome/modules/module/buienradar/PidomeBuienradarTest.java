/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.modules.module.buienradar;

import io.vertx.core.Promise;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.pidome.platform.modules.weather.Weather;
import org.pidome.platform.modules.weather.WeatherData;
import org.pidome.platform.presentation.input.fields.SelectInput;

/**
 *
 * @author john.sirach
 */
public class PidomeBuienradarTest {

    public PidomeBuienradarTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of getModuleInfo method, of class PidomeBuienradar.
     */
    @Test
    @SuppressWarnings("unchecked")
    public void genericFetchTest() throws Exception {
        PidomeBuienradar buienradar = new PidomeBuienradar();
        buienradar.getProsumer().setProducer(BuienradarHelperTest.mockedProducer());
        Promise<BuienradarConfigurator> prms = Promise.promise();
        buienradar.composeConfiguration(prms);
        BuienradarConfigurator configurator = prms.future().result();
        configurator.getSections().stream().flatMap(section -> section.getElements().stream()).forEach(inputField -> {
            if (inputField.getId().equals("stationselect")) {
                ((SelectInput<String>) inputField).setValue("Hoek van Holland");
            }
        });
        Promise<Void> confPrms = Promise.promise();
        buienradar.configure(configurator, confPrms);
        Thread.sleep(5000);

        Weather weather = buienradar.getWeather();
        assertThat(weather.getDateRefreshed(), is(notNullValue()));

        for (WeatherData weatherDay : buienradar.getDailyForecast()) {
            assertThat(weatherDay.getDateRefreshed(), is(notNullValue()));
        }

        buienradar.stopModule(Promise.promise());
    }

}
