/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.modules.module.buienradar;

import java.net.URI;
import java.net.http.HttpRequest;
import java.util.List;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.pidome.platform.hardware.driver.network.NetworkConnectionProducer;
import org.pidome.platform.modules.weather.WeatherData;

/**
 *
 * @author john.sirach
 */
public class BuienradarHelperTest {

    public BuienradarHelperTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of getWeatherStations method, of class BuienradarHelper.
     */
    @Test
    public void testGetWeatherStations() throws Exception {
        System.out.println("getWeatherStations");
        BuienradarHelper buienradarHelper = new BuienradarHelper();
        List<WeatherStation> result = buienradarHelper.getWeatherStations(mockedProducer());
        assertThat(result, is(notNullValue()));
    }

    /**
     * Test of getWeatherIcon method, of class BuienradarHelper.
     */
    @Test
    public void testGetWeatherIcon() {
        System.out.println("getWeatherIcon");
        String id = "";
        WeatherData.Icon expResult = WeatherData.Icon.NOT_AVAILABLE;
        WeatherData.Icon result = BuienradarHelper.getWeatherIcon(id);
        assertEquals(expResult, result);
    }

    /**
     * Helper.
     *
     * @return Mocker producer.
     * @throws Exception When failing.
     */
    protected static NetworkConnectionProducer mockedProducer() throws Exception {
        NetworkConnectionProducer producer = mock(NetworkConnectionProducer.class);

        when(producer.newHttpClientBuilder(any(), anyString(), any(), anyString())).thenReturn(
                HttpRequest.newBuilder(new URI(BuienradarHelper.FETCH_FROM))
        );
        return producer;
    }

}
