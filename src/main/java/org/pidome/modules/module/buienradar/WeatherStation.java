/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.modules.module.buienradar;

import java.io.Serializable;

/**
 * A weather station.
 *
 * @author john.sirach
 */
public class WeatherStation implements Serializable {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The station code.
     */
    private String stationCode;

    /**
     * The station name.
     */
    private String stationName;

    /**
     * The station code.
     *
     * @return the stationCode
     */
    public String getStationCode() {
        return stationCode;
    }

    /**
     * The station code.
     *
     * @param code the stationCode to set
     */
    public void setStationCode(final String code) {
        this.stationCode = code;
    }

    /**
     * The station name.
     *
     * @return the stationName
     */
    public String getStationName() {
        return stationName;
    }

    /**
     * The station name.
     *
     * @param name the stationName to set
     */
    public void setStationName(final String name) {
        this.stationName = name;
    }

}
