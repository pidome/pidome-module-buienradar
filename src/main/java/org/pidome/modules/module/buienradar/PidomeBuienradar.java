/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.modules.module.buienradar;

import io.vertx.core.Promise;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.platform.hardware.driver.Transport;
import org.pidome.platform.hardware.driver.network.NetworkProsumer;
import org.pidome.platform.modules.PidomeModule;
import org.pidome.platform.modules.weather.Weather;
import org.pidome.platform.modules.weather.WeatherForecastDays;
import org.pidome.platform.modules.weather.WeatherModule;
import org.pidome.platform.modules.weather.WeatherModule.Capabilities;
import org.pidome.platform.presentation.Presentation;
import org.pidome.platform.presentation.components.list.DescriptionList;
import org.pidome.platform.presentation.components.list.DescriptionListItem;
import org.pidome.platform.presentation.input.InputSection;
import org.pidome.platform.presentation.input.fields.SelectInput;
import org.pidome.tools.datetime.DateTimeUtil;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * The PiDome native supported buienradar module.
 *
 * @author john.sirach
 */
@PidomeModule(
        name = "PiDome Buienradar",
        description = "PiDome implementation for the dutch buienradar weather support.",
        transport = Transport.SubSystem.NETWORK
)
@SuppressWarnings({"checkstyle:MagicNumber"})
public class PidomeBuienradar extends WeatherModule<NetworkProsumer, BuienradarConfigurator> {

    /**
     * Url to the data owner.
     */
    private static final String DATA_OWNER_URL = "http://www.buienradar.nl";

    /**
     * Name of the data owner.
     */
    private static final String DATA_OWNER_NAME = "Buienradar.nl";

    /**
     * The station selection id in the configuration.
     */
    private static final String CONFIGURATION_STATION_ID = "stationselect";

    /**
     * Class logger.
     */
    private static final Logger LOG = LogManager.getLogger(PidomeBuienradar.class);

    /**
     * The weather station used.
     */
    private String weatherStation = "";

    /**
     * Name of the weather station.
     */
    private String weatherStationName = "Unknown";

    /**
     * The last date known.
     */
    private LocalDateTime lastWeatherDate;

    /**
     * Executor for the fetching runnable.
     */
    private ScheduledExecutorService dataFetchExecutor;

    /**
     * The helper.
     */
    private final BuienradarHelper buienradarHelper = new BuienradarHelper();

    /**
     * Module constructor.
     */
    public PidomeBuienradar() {
        super(Capabilities.CURRENT, Capabilities.FORECAST_DAY);
        this.setDataOwner(DATA_OWNER_URL, DATA_OWNER_NAME);
    }

    /**
     * Supply information about the running module.
     *
     * @return A presentation about the running module.
     */
    @Override
    public Presentation getModuleInfo() {
        Presentation presentation = new Presentation();

        DescriptionList stationInfo = new DescriptionList("Station information");
        stationInfo.addListItem(new DescriptionListItem("Weather date", this.lastWeatherDate != null
                ? DateTimeUtil.formatMediumDateTime(this.lastWeatherDate) : ""));
        stationInfo.addListItem(new DescriptionListItem("Station code", this.weatherStation));
        stationInfo.addListItem(new DescriptionListItem("Station name", this.weatherStationName));

        DescriptionList fetchInfo = new DescriptionList("Fetch information");
        fetchInfo.addListItem(new DescriptionListItem("Last attempt", buienradarHelper.getLastFetchAttempt() != null
                ? DateTimeUtil.formatMediumDateTime(buienradarHelper.getLastFetchAttempt()) : "Unknown"));
        fetchInfo.addListItem(new DescriptionListItem("Cache date", buienradarHelper.getLastCacheRefresh() != null
                ? DateTimeUtil.formatMediumDateTime(buienradarHelper.getLastCacheRefresh()) : "Unknown"));
        fetchInfo.addListItem(new DescriptionListItem("Fetch from", BuienradarHelper.FETCH_FROM));

        presentation.addSections(stationInfo, fetchInfo);
        return presentation;
    }

    /**
     * Called when the module is supposed to start.
     *
     * @param prms Promise to complete when successfully started otherwise fail.
     */
    @Override
    public void startModule(final Promise prms) {
        if (!this.weatherStation.isBlank()) {
            startFetcher();
        }
        prms.complete();
    }

    /**
     * Called when the module is supposed to stop.
     *
     * @param prms Promise to complete when succesfully stopped, otherwise fail.
     */
    @Override
    public void stopModule(final Promise prms) {
        stopFetcher();
        prms.complete();
    }

    /**
     * Stops the fetching.
     */
    private void stopFetcher() {
        if (dataFetchExecutor != null && !dataFetchExecutor.isShutdown()) {
            dataFetchExecutor.shutdownNow();
            LOG.info("Stopped buienradar fetcher at [{}]", LocalDateTime.now());
        }
    }

    /**
     * Called when the server needs a configuration.
     *
     * @param prms Complete to signal the configuration is created.
     */
    @Override
    public void composeConfiguration(final Promise<BuienradarConfigurator> prms) {
        final BuienradarConfigurator configurator = new BuienradarConfigurator();

        InputSection section = new InputSection("Resource selection", "Select the resource the weather information should originate from.");
        SelectInput<String> inputSelectStation = new SelectInput<>(CONFIGURATION_STATION_ID, "Select weather station", "Select a weather station closest to you");
        List<WeatherStation> stations = buienradarHelper.getWeatherStations(this.getProsumer().getProducer());
        inputSelectStation.setListValues(
                stations.stream()
                        .map(station -> {
                            if (this.weatherStation != null && !this.weatherStation.isBlank()
                                    && station.getStationCode().equals(this.weatherStation)) {
                                inputSelectStation.setValue(station.getStationName());
                            }
                            return station.getStationName();
                        })
                        .collect(Collectors.toList())
        );
        section.addElements(inputSelectStation);
        configurator.addSection(section);

        prms.complete(configurator);
    }

    /**
     * Posted by the server when the configuration has been done.
     *
     * @param configurator The input form of the configuration.
     * @param prms The promise to complete when done configuring.
     */
    @Override
    @SuppressWarnings("unchecked")
    public void configure(final BuienradarConfigurator configurator, final Promise<Void> prms) {
        stopFetcher();
        configurator.getSections().stream().flatMap(section -> section.getElements().stream()).forEach(inputField -> {
            if (inputField.getId().equals(CONFIGURATION_STATION_ID)) {
                String selectedStation = ((SelectInput<String>) inputField).getValue();
                buienradarHelper.getWeatherStations(this.getProsumer().getProducer()).stream()
                        .filter(station -> station.getStationName().equals(selectedStation))
                        .findFirst()
                        .ifPresent(station -> {
                            this.weatherStation = station.getStationCode();
                            this.weatherStationName = station.getStationName();
                            this.setLocationName(this.weatherStationName);
                            startFetcher();
                        });
            }
        });
        prms.complete();
    }

    /**
     * Starts the fetching routine.
     */
    private void startFetcher() {
        if (this.weatherStation != null && !this.weatherStation.isBlank()) {
            Runnable run = () -> {
                try {
                    Document document = BuienradarHelper.getRemoteXml(
                            BuienradarHelper.getHttpRequestBuilder(this.getProsumer().getProducer())
                    );
                    XPathFactory xPathfactory = XPathFactory.newInstance();
                    XPath xpath = xPathfactory.newXPath();
                    XPathExpression expr = xpath.compile("/buienradarnl/weergegevens/actueel_weer/weerstations/weerstation[@id='" + this.weatherStation + "']");

                    Node station = (Node) expr.evaluate(document, XPathConstants.NODE);
                    this.setWeather(getCurrentDataFromDocumentNode(station));
                    try {
                        this.lastWeatherDate = this.getWeather().getWeatherDate();
                    } catch (Exception ex) {
                        LOG.error("Unable to set weather date", ex);
                    }
                    XPathFactory xPathfactory2 = XPathFactory.newInstance();
                    XPath xpath2 = xPathfactory2.newXPath();
                    XPathExpression expr2 = xpath2.compile("/buienradarnl/weergegevens/verwachting_meerdaags/*");

                    NodeList stationfuture = (NodeList) expr2.evaluate(document, XPathConstants.NODESET);
                    this.setDailyForecast(getDailyForecast(stationfuture));

                    this.broadcastNewCurrent(this.getWeather());

                } catch (IOException | ParserConfigurationException | SAXException | XPathExpressionException ex) {
                    LOG.error("Problem getting/setting forecast weather data: {}", ex.getMessage(), ex);
                }

            };
            if (dataFetchExecutor == null || dataFetchExecutor.isTerminated()) {
                dataFetchExecutor = Executors.newSingleThreadScheduledExecutor(r -> new Thread(r, "Buienradar fetcher"));
                dataFetchExecutor.scheduleAtFixedRate(run, 0, 15, TimeUnit.MINUTES);
                LOG.info("Started buienradar fetcher at [{}]", LocalDateTime.now());
            }
        }
    }

    /**
     * Set the current data.
     *
     * @param station The station node to get the data from.
     * @return The current weather data.
     */
    private static Weather getCurrentDataFromDocumentNode(final Node station) {
        final Weather data = new Weather();
        if (station.hasChildNodes()) {
            for (int j = 0; j < station.getChildNodes().getLength(); j++) {
                if (station.getChildNodes().item(j).getNodeType() == Node.ELEMENT_NODE) {
                    switch (station.getChildNodes().item(j).getNodeName()) {
                        case "datum":
                            DateTimeFormatter df = DateTimeFormatter.ofPattern("MM/dd/yyyy kk:mm:ss", Locale.getDefault());
                            try {
                                LocalDateTime d = LocalDateTime.parse(station.getChildNodes().item(j).getTextContent(), df);
                                data.setWeatherDate(d);
                            } catch (Exception ex) {
                                LOG.warn("Setting local time/data for weather ({}), setting to now", ex.getMessage());
                                data.setWeatherDate(LocalDateTime.now(df.getZone()));
                            }
                            break;
                        case "luchtvochtigheid":
                        try {
                            data.setHumidity(Integer.parseInt(station.getChildNodes().item(j).getTextContent()));
                        } catch (NumberFormatException | DOMException ex) {
                            LOG.warn("Could not set humidity in forecast", ex);
                            data.setHumidity(0);
                        }
                        break;
                        case "temperatuurGC":
                        try {
                            data.setTemperature(Float.parseFloat(station.getChildNodes().item(j).getTextContent()));
                        } catch (NumberFormatException | DOMException ex) {
                            LOG.warn("Could not set temperature in forecast", ex);
                            data.setTemperature(0);
                        }
                        break;
                        case "windsnelheidMS":
                        try {
                            data.setWindSpeed(Float.parseFloat(station.getChildNodes().item(j).getTextContent()));
                        } catch (NumberFormatException | DOMException ex) {
                            LOG.warn("Could not set wind speed in forecast", ex);
                            data.setWindSpeed(0);
                        }
                        break;
                        case "windstotenMS":
                        try {
                            data.setWindGusts(Float.parseFloat(station.getChildNodes().item(j).getTextContent()));
                        } catch (NumberFormatException | DOMException ex) {
                            LOG.warn("Could not set wind gusts in forecast", ex);
                            data.setWindGusts(0);
                        }
                        break;
                        case "windrichtingGR":
                        try {
                            data.setWindDirectionDegrees(Float.parseFloat(station.getChildNodes().item(j).getTextContent()));
                        } catch (NumberFormatException | DOMException ex) {
                            LOG.warn("Could not set wind direction in forecast", ex);
                            data.setWindDirectionDegrees(0);
                        }
                        break;
                        case "windrichting":
                        try {
                            data.setWindDirection(station.getChildNodes().item(j).getTextContent());
                        } catch (DOMException ex) {
                            LOG.warn("Could not set wind direction in forecast", ex);
                            data.setWindDirection("");
                        }
                        break;
                        case "luchtdruk":
                        try {
                            data.setPressure(Float.parseFloat(station.getChildNodes().item(j).getTextContent()));
                        } catch (NumberFormatException | DOMException ex) {
                            LOG.warn("Could not set pressure in forecast", ex);
                            data.setPressure(0);
                        }
                        break;
                        case "icoonactueel":
                            data.setStateIcon(BuienradarHelper.getWeatherIcon(station.getChildNodes().item(j).getAttributes().getNamedItem("ID").getTextContent()));
                            data.setStateDescription(station.getChildNodes().item(j).getAttributes().getNamedItem("zin").getTextContent());
                            break;
                        default:
                            // No action needed.
                            break;
                    }
                }
            }
        }
        data.setDateRefreshed(LocalDateTime.now());
        return data;
    }

    /**
     * Returns a daily forecast.
     *
     * @param stationfuture Allthough defined as a station, the node with daily
     * data.
     * @return The daily data.
     */
    private static WeatherForecastDays getDailyForecast(final NodeList stationfuture) {
        WeatherForecastDays multiDayForecast = new WeatherForecastDays();

        for (int j = 0; j < stationfuture.getLength(); j++) {
            Node futureNode = stationfuture.item(j);
            if (futureNode.getNodeType() == Node.ELEMENT_NODE) {
                switch (futureNode.getNodeName()) {
                    case "dag-plus1":
                    case "dag-plus2":
                    case "dag-plus3":
                    case "dag-plus4":
                    case "dag-plus5":
                        if (futureNode.hasChildNodes()) {
                            Weather futureData = new Weather();
                            futureData.setDateRefreshed(LocalDateTime.now());
                            for (int k = 0; k < futureNode.getChildNodes().getLength(); k++) {
                                switch (futureNode.getChildNodes().item(k).getNodeName()) {
                                    case "datum":
                                        DateTimeFormatter df = DateTimeFormatter.ofPattern("EEEE d MMM u", new Locale("nl")); ////donderdag 15 januari 2015
                                        try {
                                            LocalDate d = LocalDate.parse(putDotAfterShortDate(futureNode.getChildNodes().item(k).getTextContent()), df);
                                            futureData.setWeatherDate(d.atStartOfDay());
                                        } catch (Exception ex) {
                                            LOG.warn("Could not set future weather date, setting now", ex);
                                            futureData.setWeatherDate(LocalDate.now(df.getZone()).atStartOfDay());
                                        }
                                        break;
                                    case "maxtemp":
                                        futureData.setTemperature(Float.parseFloat(futureNode.getChildNodes().item(k).getTextContent()));
                                        break;
                                    case "windrichting":
                                        futureData.setWindDirection(futureNode.getChildNodes().item(k).getTextContent());
                                        break;
                                    case "windkracht":
                                        futureData.setWindSpeed(Float.parseFloat(futureNode.getChildNodes().item(k).getTextContent()));
                                        break;
                                    case "icoon":
                                        futureData.setStateIcon(BuienradarHelper.getWeatherIcon(futureNode.getChildNodes().item(k).getAttributes().getNamedItem("ID").getTextContent()));
                                        break;
                                    default:
                                        /// no action needed.
                                        break;
                                }
                            }
                            multiDayForecast.add(futureData);
                        }
                        break;
                    default:
                        // No action.
                        break;
                }
            }
        }
        return multiDayForecast;
    }

    /**
     * Sometimes......
     *
     * @param input The partial month of a formatted date
     * @return the expected date format.
     */
    private static String putDotAfterShortDate(final String input) {
        return input.replace("jan", "jan.")
                .replace("feb", "feb.")
                .replace("mar", "mar.")
                .replace("apr", "apr.")
                .replace("mei", "mei")
                .replace("jun", "jun.")
                .replace("jul", "jul.")
                .replace("aug", "aug.")
                .replace("sep", "sep.")
                .replace("okt", "okt.")
                .replace("nov", "nov.")
                .replace("dec", "dec.");
    }

}
