/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.modules.module.buienradar;

import java.io.IOException;
import java.io.StringReader;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.platform.hardware.driver.network.NetworkConnectionProducer;
import org.pidome.platform.modules.weather.WeatherData;
import org.pidome.tools.datetime.DateTimeUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * Helper class for buienradar.
 *
 * Helper class for fetching the buienradar data.
 *
 * There are statics used because if we start multiple fetchers they can re-use
 * the same data as buienradar provides ALL data in a single call.
 *
 * @author john.sirach
 */
public final class BuienradarHelper {

    /**
     * Class logger.
     */
    private static final Logger LOG = LogManager.getLogger(PidomeBuienradar.class);

    /**
     * The timout for connecting to the remote service.
     */
    private static final int REMOTE_SERVICE_TIMEOUT = 5000;

    /**
     * Cache of weather stations.
     */
    private final List<WeatherStation> knownStations = new ArrayList<>();

    /**
     * Last cache refresh date.
     */
    private static AtomicReference<LocalDateTime> atomicLastCacheRefresh = new AtomicReference<>();

    /**
     * Last cache refresh date.
     */
    private static AtomicReference<LocalDateTime> atomicLastAttempt = new AtomicReference<>();

    /**
     * The cached buienradar document.
     */
    private static AtomicReference<Document> cachedDcoument = new AtomicReference<>();

    /**
     * 15 Minutes.
     */
    private static final int MINUTES_14 = 14;

    /**
     * Protocol used to fetch.
     */
    protected static final String FETCH_PROTOCOL = "https";

    /**
     * The host to fetch from.
     */
    protected static final String FETCH_HOST = "data.buienradar.nl";

    /**
     * The path to fetch from.
     */
    protected static final String FETCH_PATH = "/1.0/feed/xml";

    /**
     * Full fetch path.
     */
    protected static final String FETCH_FROM = FETCH_PROTOCOL + "://" + FETCH_HOST + FETCH_PATH;

    /**
     * Returns the last cache refresh date.
     *
     * @return The last cache refresh date.
     */
    protected static LocalDateTime getLastCacheRefresh() {
        return atomicLastCacheRefresh.get();
    }

    /**
     * Sets the last known cache refresh.
     *
     * @param cacheRefresh The moment the last cache was refreshed.
     */
    protected static void setLastCacheRefresh(final LocalDateTime cacheRefresh) {
        atomicLastCacheRefresh.set(cacheRefresh);
    }

    /**
     * The last attempt date.
     *
     * @return The last retrieve attempt date.
     */
    protected static LocalDateTime getLastFetchAttempt() {
        return atomicLastAttempt.get();
    }

    /**
     * Set the last attempt date.
     *
     * @param fetchAttempt Sets the last fetch attempt.
     */
    protected static void setLastFetchAttempt(final LocalDateTime fetchAttempt) {
        atomicLastAttempt.set(fetchAttempt);
    }

    /**
     * Returns a lis of existing weather stations.
     *
     * @param producer The producer of the network connection.
     * @return List of weather stations.
     */
    protected List<WeatherStation> getWeatherStations(final NetworkConnectionProducer producer) {
        // CPD-OFF
        try {

            Document document = getRemoteXml(getHttpRequestBuilder(producer));
            knownStations.clear();

            XPathFactory xPathfactory = XPathFactory.newInstance();
            XPath xpath = xPathfactory.newXPath();
            XPathExpression expr = xpath.compile("/buienradarnl/weergegevens/actueel_weer/weerstations/weerstation");

            NodeList stations = (NodeList) expr.evaluate(document, XPathConstants.NODESET);

            for (int i = 0; i < stations.getLength(); i++) {
                Node station = stations.item(i);
                String id = "";
                String name = "";
                if (station.hasChildNodes()) {
                    for (int j = 0; j < station.getChildNodes().getLength(); j++) {
                        if (station.getChildNodes().item(j).getNodeType() == Node.ELEMENT_NODE) {
                            switch (station.getChildNodes().item(j).getNodeName()) {
                                case "stationcode":
                                    id = station.getChildNodes().item(j).getTextContent();
                                    break;
                                case "stationnaam":
                                    name = station.getChildNodes().item(j).getTextContent().replace("Meetstation ", "");
                                    break;
                                default:
                                    // No action needed.
                                    break;
                            }
                        }
                    }
                }
                if (!id.isEmpty() && !name.isEmpty()) {
                    final WeatherStation weatherStation = new WeatherStation();
                    weatherStation.setStationCode(id);
                    weatherStation.setStationName(name);
                    knownStations.add(weatherStation);
                }
            }
            Collections.sort(knownStations, (s1, s2) -> {
                return s1.getStationName().compareTo(s2.getStationName());
            });
        } catch (IOException | ParserConfigurationException | SAXException | XPathExpressionException ex) {
            LOG.error("Could not get Buienradar weather stations: {}", ex.getMessage(), ex);
        }
        return knownStations;
        // CPD-ON
    }

    /**
     * Returns the XML document from buienradar service to work on.
     *
     * @param builder The request builder of the network connection.
     * @return The XML document from buienradar.
     * @throws MalformedURLException When the url to buienradar is incorrect.
     * @throws IOException When unable to connect to the buienradar service.
     * @throws ParserConfigurationException When creating the document builder
     * fails.
     * @throws SAXException When parsing fails.
     */
    protected static Document getRemoteXml(final HttpRequest.Builder builder) throws MalformedURLException, IOException, ParserConfigurationException, SAXException {
        setLastFetchAttempt(LocalDateTime.now());
        LocalDateTime lastAttempt = getLastFetchAttempt();
        LocalDateTime lastCacheRefresh = getLastCacheRefresh();

        if (lastAttempt == null || lastCacheRefresh == null || cachedDcoument.get() == null || ChronoUnit.MINUTES.between(lastAttempt.minusMinutes(MINUTES_14), lastCacheRefresh) <= 0) {
            try {
                HttpClient client = HttpClient.newBuilder().build();
                HttpRequest request = builder.timeout(Duration.ofMillis(REMOTE_SERVICE_TIMEOUT)).build();
                HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
                String remoteData = response.body();
                if (LOG.isDebugEnabled()) {
                    LOG.debug("Received document: [{}]", remoteData);
                }
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                DocumentBuilder docBuilder = factory.newDocumentBuilder();
                cachedDcoument.set(docBuilder.parse(new InputSource(new StringReader(remoteData))));
                LOG.info("Buienradar data refreshed at: [{}]", DateTimeUtil.formatMediumDateTime(lastAttempt));
                setLastCacheRefresh(LocalDateTime.now());
            } catch (InterruptedException ex) {
                LOG.error("Unable to get remote document [{}]", ex.getMessage(), ex);
                return null;
            }
        } else {
            LOG.info("Returning buienradar data from cache [{}]", DateTimeUtil.formatMediumDateTime(lastCacheRefresh));
        }
        return cachedDcoument.get();
    }

    /**
     * Map weather status to an icon.
     *
     * @param id Id identifying the icon.
     * @return The icon used.
     */
    protected static WeatherData.Icon getWeatherIcon(final String id) {
        switch (id) {
            case "a":
                return WeatherData.Icon.CLEAR;
            case "aa":
                return WeatherData.Icon.CLEAR_NIGHT;
            case "b":
                return WeatherData.Icon.PARTLY_CLEAR;
            case "bb":
                return WeatherData.Icon.PARTLY_CLEAR_NIGHT;
            case "c":
            case "cc":
                return WeatherData.Icon.DREARY;
            case "d":
            case "dd":
                return WeatherData.Icon.FOG;
            case "f":
            case "ff":
                return WeatherData.Icon.SHOWERS;
            case "g":
            case "gg":
                return WeatherData.Icon.THUNDERSTORMS;
            case "h":
                return WeatherData.Icon.MOSTLY_CLOUDY_SHOWERS;
            case "hh":
                return WeatherData.Icon.MOSTLY_CLOUDY_SHOWERS_NIGHT;
            case "i":
                return WeatherData.Icon.MOSTLY_CLOUDY_SNOW;
            case "ii":
                return WeatherData.Icon.MOSTLY_CLOUDY_SNOW_NIGHT;
            case "m":
                return WeatherData.Icon.MOSTLY_CLOUDY_SHOWERS;
            case "mm":
                return WeatherData.Icon.MOSTLY_CLOUDY_SHOWERS_NIGHT;
            case "n":
                return WeatherData.Icon.HAZE;
            case "nn":
                return WeatherData.Icon.HAZE_NIGHT;
            case "o":
                return WeatherData.Icon.MOSTLY_CLEAR;
            case "oo":
                return WeatherData.Icon.MOSTLY_CLEAR_NIGHT;
            case "p":
                return WeatherData.Icon.PARTLY_CLEAR;
            case "pp":
                return WeatherData.Icon.PARTLY_CLEAR_NIGHT;
            case "q":
            case "qq":
                return WeatherData.Icon.RAIN;
            case "s":
            case "ss":
                return WeatherData.Icon.THUNDERSTORMS;
            case "t":
            case "tt":
                return WeatherData.Icon.SNOW;
            case "u":
                return WeatherData.Icon.MOSTLY_CLOUDY_SNOW;
            case "uu":
                return WeatherData.Icon.MOSTLY_CLOUDY_SNOW_NIGHT;
            case "w":
            case "ww":
                return WeatherData.Icon.RAIN_AND_SNOW;
            default:
                return WeatherData.Icon.NOT_AVAILABLE;
        }
    }

    /**
     * Returns a new http builder for fetching the data.
     *
     * @param producer The producer for the network connection.
     * @return The builder with the minimal requirements to fetch the data.
     * @throws UnknownHostException When the given host does not exist.
     */
    protected static HttpRequest.Builder getHttpRequestBuilder(final NetworkConnectionProducer producer) throws UnknownHostException {
        return producer.newHttpClientBuilder(
                HttpRequest.newBuilder(),
                BuienradarHelper.FETCH_PROTOCOL,
                InetAddress.getByName(BuienradarHelper.FETCH_HOST),
                BuienradarHelper.FETCH_PATH);
    }

}
